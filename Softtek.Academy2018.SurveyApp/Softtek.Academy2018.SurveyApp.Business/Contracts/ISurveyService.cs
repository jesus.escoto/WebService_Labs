﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    public interface ISurveyService : IGenericService<Survey>
    {
        ICollection<Survey> GetLast();

    }
}
