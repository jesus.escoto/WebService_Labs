﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _dataRepository;

        public SurveyService(ISurveyRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        int IGenericService<Survey>.Add(Survey survey)
        {
            if (survey.Id != 0) return 0;

            if (string.IsNullOrEmpty(survey.Title) || string.IsNullOrEmpty(survey.Description)) return 0;

            bool UserNameExist = _dataRepository.Exist(survey.Title);

            if (UserNameExist) return 0;

            int id = _dataRepository.Add(survey);

            return id;

        }

        bool IGenericService<Survey>.Delete(Survey survey)
        {
            if (survey.Id <= 0) return false;

            Survey currentSurvey = _dataRepository.Get(survey.Id);

            if (currentSurvey == null || !currentSurvey.IsArchived) return false;

            return _dataRepository.Delete(survey);
        }

        Survey IGenericService<Survey>.Get(int id)
        {
            if (id <= 0) return null;

            Survey survey = _dataRepository.Get(id);

            if (survey != null && !survey.IsArchived) return null;

            return survey;
        }

        ICollection<Survey> ISurveyService.GetLast()
        {

            ICollection<Survey> survey = _dataRepository.GetLast();

            if (survey != null) return null;

            survey = survey.Select(x => new Survey
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                IsArchived = x.IsArchived
            }).ToList();

            return survey;
        }

        bool IGenericService<Survey>.Update(Survey survey)
        {
            if (survey.Id <= 0) return false;

            if (string.IsNullOrEmpty(survey.Title) ||
                string.IsNullOrEmpty(survey.Description)) return false;

            bool existTitle = _dataRepository.Exist(survey.Title);

            if (!existTitle) return false;

            return _dataRepository.Update(survey);
        }
    }
}
