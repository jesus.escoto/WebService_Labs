﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyDataRepository : ISurveyRepository
    {
        public bool Exist(string title)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.Any(e => e.Title.ToLower() == title.ToLower());
            }
        }

        int IGenericRepository<Survey>.Add(Survey survey)
        {
            using (var context = new SuerveyDbContext())
            {
                survey.IsArchived = true;
                context.Surveys.Add(survey);

                context.SaveChanges();

                return survey.Id;
            }
        }

        bool IGenericRepository<Survey>.Delete(Survey survey)
        {
            using (var context = new SuerveyDbContext())
            {
                Survey currentSurvey = context.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == survey.Id);

                if (currentSurvey == null) return false;

                currentSurvey.IsArchived = false;

                context.Entry(currentSurvey).State = EntityState.Deleted;

                context.SaveChanges();

                return true;
            }
        }

        Survey IGenericRepository<Survey>.Get(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        ICollection<Survey> ISurveyRepository.GetLast()
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.Reverse().Take(10).Reverse().ToList();
            }
        }

        bool IGenericRepository<Survey>.Update(Survey survey)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Survey currentSurvey = ctx.Surveys.SingleOrDefault(x => x.Id == survey.Id);

                if (currentSurvey == null) return false;

                currentSurvey.Title = survey.Title;

                currentSurvey.Description = survey.Description;

                currentSurvey.IsArchived = survey.IsArchived;

                ctx.Entry(currentSurvey).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }
    }
}
