﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/survey")]
    public class SurveyController : ApiController
    {


        private readonly ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] SurveyDTO surveyDTO)
        {
            if (surveyDTO == null) return BadRequest("Request is null");

            Survey survey = new Survey
            {
                Title = surveyDTO.Title,

                Description = surveyDTO.Description

            };

            int id = _surveyService.Add(survey);

            if (id <= 0) return BadRequest("Unable to create survey");

            var payload = new { SurveyId = id };

            return Ok(payload);
        }
        
    }
}
