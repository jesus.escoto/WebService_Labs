﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Models
{
    public class SurveyDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsArchived { get; set; }
    }
}